process.env.VUE_APP_VERSION = require('./package.json').version
const now = new Date(Date.now())
process.env.VUE_APP_BUILDTIME= now.toLocaleDateString("en-GB", {timeZone: "Europe/Zurich",year: "numeric", month: "long", day: "numeric"})
module.exports = {
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },
  transpileDependencies: [
    'quasar'
  ],
}
